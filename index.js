const express = require('express');
const fetch = require('node-fetch');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Starting server on http://127.0.0.1:${port}`)
    console.log(`Starting server at localhost: ${port}`);
});

//Middleware
app.use(express.static('public'));
app.use(express.json())

//APIs
app.get('/gifs/:begriff', async (request, response) => {
    const begriff = request.params.begriff;
    const api_key = process.env.API_KEY_GIPHY
    const gifs_url = `http://api.giphy.com/v1/gifs/search?q=${begriff}&rating=g&api_key=${api_key}`;
    const gifs_response = await fetch(gifs_url);
    const gifs_data = await gifs_response.json();

    response.json(gifs_data);
});

app.get('/trending', async (request, response) => {
    const api_key = process.env.API_KEY_GIPHY
    const trending_url = `http://api.giphy.com/v1/gifs/trending?limit=1&rating=g&api_key=${api_key}`;
    const trending_response = await fetch(trending_url);
    const trending_data = await trending_response.json();

    response.json(trending_data);
});

app.get('/randomGIF', async (request, response) => {
    const api_key = process.env.API_KEY_GIPHY
    const random_url = `http://api.giphy.com/v1/gifs/random?rating=g&api_key=${api_key}`;
    const random_response = await fetch(random_url);
    const random_data = await random_response.json();

    response.json(random_data);
});

app.get('/stock/:begriff', async (request, response) => {
    const api_key = process.env.API_KEY_PIXABAY
    const begriff = request.params.begriff;
    const stock_url = `https://pixabay.com/api/?q=${begriff}&per_page=25&key=${api_key}`
    const stock_response = await fetch(stock_url);
    const stock_data = await stock_response.json();
    response.json(stock_data);
});

//Error Handling
app.use(function(req, res){
    res.type('text/plain');
    res.status(404);
    res.send('404 - Not Found');
})

app.use(function(err, req, res, next){
    console.error(err, stack);
    res.type('text/plain');
    res.status(500);
    res.send('500 - Internal Error');
})
