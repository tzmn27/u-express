U-Express
=========

Webbasierte Systeme Hausübung #2

Prof. Dr. Klaus Quibeldey-Cirkel

Die Website können Sie <a href="https://u-express.herokuapp.com/">hier</a> erreichen.

Einführung
---------

Über U-Express haben Sie nun die Möglichkeit nach GIFS oder Stockfotos zu suchen, um sich visuell besser auszudrücken. Sie können beides auf einer Website tun! Sehen Sie dort den
aktuellen Platz 1 der GIF Trends auf GIPHY, oder lassen Sie sich zufällige GIFs anzeigen, solange Sie nach nichts Bestimmten suchen. Haben Sie sich für eine Kategorie
entschieden, können Sie sich durch die Suchergebnisse klicken. Auch bei den Stockfotos können Sie sich Ihren Favoriten zu Ihrem Suchbegriff aussuchen.
U-Express nutzt 4 API-Endpunkte, davon sind 3 GIPHY und einer ist Pixabay.


Installation
---------
1. Sie benötigen node.js. Falls Sie sich nicht sicher sind, ob Sie es bereits installiert haben, geben Sie in der Konsole den Befehl `node -v` ein. Wird eine Version angezeigt, haben
Sie es bereits installiert. Ansonsten können Sie node <a href="https://nodejs.org/de/">hier</a> herunterladen.

2. Klonen Sie das Projekt von GitLab, indem Sie im gewünschten Verzeichnis `git clone git@git.thm.de:tzmn27/u-express.git` ausführen.

3. Führen Sie im Zielverzeichnis den Befehl `npm install` aus.

4. Erstellen Sie eine neue Datei mit dem Namen `.env`. Dort fügen Sie Ihre Api-Schlüssel ein, wie in der Datei `.env-sample` gezeigt. Diese können Sie gratis von <a href="https://developers.giphy.com/">
GIPHY</a> und <a href="https://pixabay.com/de/service/about/api/">Pixabay</a> anfordern.

5. Starten Sie den Server mit `node index.js`. Er ist nun über <a href="http://localhost:3000/">Localhost:300</a> oder über den Link in der Konsole erreichbar. Stoppen können Sie
den Server, indem Sie in der Konsole Strg + C drücken.
Nutzen Sie Webstorm, können Sie alternativ oben rechts `index.js` auswählen und dann mit dem grünen "Play" Button den Server starten.