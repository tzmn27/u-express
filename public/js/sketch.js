const searchGIF = document.getElementById('searchButton');
const searchStock = document.getElementById('searchstock');
const nextGIF = document.getElementById('nextgif');
const prevGIF = document.getElementById('prevgif');
const nextStock = document.getElementById('nextstock');
const prevStock = document.getElementById('prevstock');
let x = 0;
let y = 0;

async function getTrendingGif(){
    try {
        const api_url = `/trending`;
        const response = await fetch(api_url);
        const json = await response.json();
        document.getElementById('trendinggif').src = json.data[0].embed_url;
        document.getElementById('trendinglink').href = json.data[0].url;
    } catch {
        alert("Ein Fehler ist aufgetreten, versuchen Sie es später erneut")
    }
}

async function getRandomGif(){
    try {
        const api_url = `/randomGIF`;
        const response = await fetch(api_url);
        const json = await response.json();
        document.getElementById('searchgif').src = json.data.embed_url;
        document.getElementById('searchlink').href = json.data.url;
    }catch{
        alert("Ein Fehler ist aufgetreten, versuchen Sie es später erneut")
    }
    try {
        const krit = "schaf";
        const api_url = `stock/${krit}`;
        const response = await fetch(api_url);
        const json = await response.json();
        document.getElementById('stockfoto').src = json.hits[0].webformatURL;
        document.getElementById('stocklink').href = json.hits[0].pageURL;
        document.getElementById('stockuserlink').href = 'https://pixabay.com/de/users/' + json.hits[0].user + '-' + json.hits[0].user_id + '/';
        document.getElementById('stockuser').textContent = json.hits[0].user;
    }catch{
        alert("Ein Fehler ist aufgetreten, versuchen Sie es später erneut")
    }
}

searchGIF.addEventListener('click', async event =>{
    try{
    let eingabe = document.getElementById('parameter').value;
    if (eingabe === ""){
        marksearchGIF();
        alert("Suchfeld muss ausgefüllt sein!");
    }else{
        const search = document.getElementById('parameter').value;
        const api_url = `gifs/${search}`;
        const response = await fetch(api_url);
        const json = await response.json();
        document.getElementById('searchgif').src = json.data[0].embed_url;
        document.getElementById('searchlink').href = json.data[0].url;
        if(document.getElementById('parameter').style.backgroundColor != "#FFFFFF") {
            document.getElementById('parameter').style.backgroundColor = "#FFFFFF"
        }
    }} catch {
        marksearchGIF();
        alert("Leider keine Suchergebnisse, versuchen Sie einen anderen Begriff");
    }
});

nextGIF.addEventListener('click', async event =>{
    const search = document.getElementById('parameter').value;
    const api_url = `gifs/${search}`;
    const response = await fetch(api_url);
    const json = await response.json();
    const length = json.data.length - 1;
    try{
    if (x < length){
        document.getElementById('searchgif').src = json.data[x + 1].embed_url;
        document.getElementById('searchlink').href = json.data[x+1].url;
        x += 1;
    }
    else{
        document.getElementById('searchgif').src = json.data[0].embed_url;
        document.getElementById('searchlink').href = json.data[0].url;
        x = 0;
    }} catch {
        marksearchGIF();
        alert("Ein Fehler ist aufgetreten, versuchen Sie es später erneut");
    }
});

prevGIF.addEventListener('click', async event =>{
    const krit = document.getElementById('parameter').value;
    const api_url = `gifs/${krit}`;
    const response = await fetch(api_url);
    const json = await response.json();
    const length = json.data.length - 1;

    try{
    if (x > 0){
        document.getElementById('searchgif').src = json.data[x-1].embed_url;
        document.getElementById('searchlink').href = json.data[x-1].url;
        x -= 1;
    }
    else{
        document.getElementById('searchgif').src = json.data[length].embed_url;
        document.getElementById('searchlink').href = json.data[length].url;
        x = length;
    }} catch {
        marksearchGIF();
        alert("Ein Fehler ist aufgetreten, versuchen Sie es später erneut");
    }
});

searchStock.addEventListener('click', async event =>{
    try{
        let eingabe = document.getElementById('stocksuche').value;
        if (eingabe === ""){
            marksearchStock();
            alert("Suchfeld muss ausgefüllt sein!");
        } else {
            const krit = document.getElementById('stocksuche').value;
            const api_url = `stock/${krit}`;
            const response = await fetch(api_url);
            const json = await response.json();
            console.log(json);

            document.getElementById('stockfoto').src = json.hits[0].webformatURL;
            document.getElementById('stocklink').href = json.hits[0].pageURL;
            document.getElementById('stockuserlink').href = 'https://pixabay.com/de/users/' + json.hits[0].user + '-' + json.hits[0].user_id + '/';
            document.getElementById('stockuser').textContent = json.hits[0].user;
            if (document.getElementById('stocksuche').style.backgroundColor = "#FFFFFF"){
                document.getElementById('stocksuche').style.backgroundColor = "#FFFFFF";
            }
        }} catch{
        marksearchStock();
        alert("Leider keine Suchergebnisse, versuchen Sie einen anderen Begriff");
    }
});

nextStock.addEventListener('click', async event =>{
    const krit = document.getElementById('stocksuche').value;
    const api_url = `stock/${krit}`;
    const response = await fetch(api_url);
    const json = await response.json();
    const length = json.hits.length - 1;

    try{
    if (y < length){
        document.getElementById('stockfoto').src = json.hits[y+1].webformatURL;
        document.getElementById('stocklink').href = json.hits[y+1].pageURL;
        document.getElementById('stockuserlink').href = 'https://pixabay.com/de/users/' + json.hits[y+1].user + '-' + json.hits[y+1].user_id + '/';
        document.getElementById('stockuser').textContent = json.hits[y+1].user;
        y += 1;
    }
    else{
        document.getElementById('stockfoto').src = json.hits[0].webformatURL;
        document.getElementById('stocklink').href = json.hits[0].pageURL;
        document.getElementById('stockuserlink').href = 'https://pixabay.com/de/users/' + json.hits[0].user + '-' + json.hits[0].user_id + '/';
        document.getElementById('stockuser').textContent = json.hits[0].user;
        y = 0;
    }} catch {
        marksearchStock();
        alert("Ein Fehler ist aufgetreten, versuchen Sie es später erneut");
    }
});

prevStock.addEventListener('click', async event =>{
    const krit = document.getElementById('stocksuche').value;
    const api_url = `stock/${krit}`;
    const response = await fetch(api_url);
    const json = await response.json();
    const length = json.hits.length - 1;

    try{
    if (y > 0){
        document.getElementById('stockfoto').src = json.hits[y-1].webformatURL;
        document.getElementById('stocklink').href = json.hits[y-1].pageURL;
        document.getElementById('stockuserlink').href = 'https://pixabay.com/de/users/' + json.hits[y-1].user + '-' + json.hits[y-1].user_id + '/';
        document.getElementById('stockuser').textContent = json.hits[y-1].user;
        y -= 1;
    }
    else{
        document.getElementById('stockfoto').src = json.hits[length].webformatURL;
        document.getElementById('stocklink').href = json.hits[length].pageURL;
        document.getElementById('stockuserlink').href = 'https://pixabay.com/de/users/' + json.hits[length].user + '-' + json.hits[length].user_id + '/';
        document.getElementById('stockuser').textContent = json.hits[length].user;
        y = length;
    }} catch {
        marksearchStock();
        alert("Leider keine Suchergebnisse, versuchen Sie einen anderen Begriff");
    }
});