(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 56)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 56
  });

  //Navbar
  $(window).scroll(function(){
    $('nav').toggleClass('scrolled', $(this).scrollTop() > 256);
  });

})(jQuery); // End of use strict

//Mark Input on Error
function marksearchGIF(){
  document.getElementById('parameter').style.backgroundColor="#ff8f8f";
  document.getElementById('parameter').focus();
}

function marksearchStock(){
  document.getElementById('stocksuche').style.backgroundColor="#ff8f8f";
  document.getElementById('stocksuche').focus();
}

//Show Prev/Next Buttons
function gifButtonDisplay(){
  let eingabe = document.getElementById('parameter').value;
  if(eingabe !== ""){
    document.getElementById('gifButtons').style.display = 'block';
    document.getElementById('shuffleButton').style.display = 'none';
    document.getElementById('text').textContent = "Klicken Sie sich doch mal durch!";
  }
}

function stockButtonDisplay(){
  let eingabe = document.getElementById('stocksuche').value;
  if(eingabe !== ""){
    document.getElementById('stockButtons').style.display = 'block';
    document.getElementById('stocktext').style.display = 'block';
  }
}